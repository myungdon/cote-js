// function solution(numbers, direction) {
//     direction === "right" ? numbers.unshift(numbers[numbers.length - 1]) : 0;
//     return numbers;
// }

// function solution(numbers, direction) {
//     if (direction === "right") {
//         numbers.unshift(numbers.pop());
//     } else numbers.push(numbers.shift())
//     return numbers;
// }
// console.log(solution([4, 455, 6, 4, -1, 45, 6], "left"));

// function solution(array) {
//     var answer = 0;
//     return answer;
// }
//
// console.log(solution([1, 2, 3, 3, 3, 4]));

// function solution(n) {
//     let answer = [];
//     for (let i= 0; i <= n; i++){
//         if (n % i === 0){
//             answer.push(i);
//         }
//     }
//     return answer;
// }
//
// function solution(n) {
//     var answer = n.map(v => v).filter;
//     return answer;
// }
//
// console.log(solution(24));

// 약수 구하기
// 24 -> [1, 2, 3, 4, 6, 8, 12, 24]
// 29 -> [1, 29]

// 숫자 찾기

// function solution(num, k) {
//     // const answer = String(num).split('').filter(v => Number(v) === k );
//     // const result = num.indexOf(answer[0]);
//     return String(num).indexOf(k) + 1
// }
//
// console.log(solution(123456, 7))

// function solution() {
//     let a = [1, 5, 45, 8, 90, 120];
//     let b = 0;
//     for (let i = 0; i <= a.length; i++ ) {
//         if (a[i] > a[i-1]) {
//             b =  a[i];
//         } else b = a[i-1];
//     }
//     return b;
// }
//
// console.log(solution())

// [1, 100].reduce((acc, currentValue) => Math.max(acc, currentValue), 50);

// 소문자로 바꾸고 오름차순
// function solution(my_string) {
//     return my_string.toLowerCase().split('').sort().join('');
// }
//
// console.log(solution("Bcad"))

// 숫자만 뽑아서 정렬하기
// function solution(my_string) {
//     return my_string.replace(/[^0-9]/g, '').split('').map(Number).sort();
// }
//
// console.log(solution("hi12392"))

// 369 게임
// function solution(order) {
//     return order.toString().split('').filter(v => v === '3' || v === '6' || v === '9').join('').length;

// return (order + '').replace(/[^3,6,9]/g, '').length
// return [...order.toString().matchAll(/[3|6|9]/g)].length;
// }
//
// console.log(solution(3))

// 직육면체에 정육면체 담기
// function solution(box, n) {
//     return Math.floor(box[0]/n) * Math.floor(box[1]/n) * Math.floor(box[2]/n);
// }
//
// console.log(solution([10, 8, 6], 3))

// 합성수 찾기
// function solution(n) {
//     let result = [];
//     for (let i = 4; i <= n; i++) {
//         if (i % 2 === 0 || i % 3 === 0 || (i > 5 && i % 5 === 0) || (i > 7 && i % 7 === 0)) {
//             result.push(i);
//         }
//     }
//     return result.length;
// }
//
// console.log(solution(15))

// 이진수의 합 구하기
// function solution(bin1, bin2) {
//     return (parseInt(bin1, 2) + parseInt(bin2, 2)).toString(2)
// }
//
// console.log(solution("10", "11"))

// 문자열 계산하기
// + - 밖에 없다
// function solution(my_string) {
//     let array = my_string.split(' ')
//     for (let i = 0; i <= my_string.length; i++)
//     return array
// }
//
// console.log(solution("3 + 4"))

// 빵 나누기
// 힙을 써야 함
// function solution(a, b) {
//     let sumA = a.reduce((a, b) => a + b, 0)
//     let sumB = b.reduce((a, b) => a + b, 0)
//     let count = 0
//
//     while (a.length > 0 && b.length > 0) {
//         if (sumA === sumB) {
//             return count;
//         } else if (sumA > sumB) {
//             let popValue = a.shift();
//             b.push(popValue);
//             sumA -= popValue;
//             sumB += popValue;
//         } else {
//             let popValue = b.shift();
//             a.push(popValue);
//             sumA += popValue;
//             sumB -= popValue;
//         }
//         count++;
//     }
//
//     return sumA === sumB ? count : -1;
// }
//
// // 한쪽으로 다 모은다
// // 두 배열을 비교해서 옮긴다.
// // a의 가장 큰수와 b의 가장 작은 수를 비교
//
//
// console.log(solution([1, 2, 3, 4], [5, 6, 7, 8]))

// 빵 나누기 최적화 (시간 복잡도)
// function solution(a, b) {
//     const totalList = [...a, ...b]
//     const totalSum = totalList.reduce((acc, val) => acc + val, 0)
//
//     let aSum = a.reduce((acc, val) => acc + val, 0)
//
//     let leftPointer = 0
//     let rightPointer = a.length - 1
//     let count = 0
//
//     while (leftPointer <= rightPointer) {
//         if (totalSum - aSum === aSum) return count
//         if (totalSum - aSum > aSum) {
//             rightPointer++
//             aSum += totalList[rightPointer]
//             count++
//         } else {
//             aSum -= totalList[leftPointer]
//             leftPointer++
//             count++
//         }
//     }
//     return -1;
// }
//
//
//
// 4
// 12345678, 36 전체배열합친거, sum
// 1234, 10 a배열, a배열sum
//
// a배열과 b배열을 비교함
//
// 12345
// count + 1
//
//
// 123456
// count +1
//
//
// 23456
//
// count +1
//
// 3456, 18
//
// console.log(solution([1, 2, 3, 4], [6, 5, 7, 8]))

// 끗말 잇기
// function solution(a) {
//     let words = []
//     words.push(a)
//     return words[words.length - 1].slice(words[words.length - 1].length - 1)
// }
//
// console.log(solution('산기슭곰발'))

// 구슬을 나누는 경우의 수
// function solution(balls, share) {
//     return Math.floor(fact(balls, 1) / fact(balls - share, 1) / fact(share, 1))
// }
//
// function fact(a, b) {
//     return a === 0 ? b : fact(a - 1, a * b)
// }
//
// console.log(solution(30, 15))

// 문자열 계산하기
// function solution(my_string) {
//     let array = my_string.split(' ')
//     let a = Number(array[0])
//
//     for (let i = 1; i <= array.length; i++) {
//         if (array[i] === '+') {
//             a += Number(array[i+1])
//         } else if (array[i] === '-') {
//             a -= Number(array[i+1])
//         }
//     }
//
//     return a
// }
// console.log(solution("3 + 4"))

// 영어가 싫어요
// function solution(numbers) {
//     return numbers.replace(/(zero)/g,"0").replace(/(one)/g,"1").replace(/(two)/g, "2").replace(/(three)/g, "3").replace(/(four)/g, "4").replace(/(five)/g, "5").replace(/(six)/g, "6").replace(/(seven)/g, "7").replace(/(eight)/g, "8").replace(/(nine)/g, "9");
// }
//
// console.log(solution("onetwothreefourfivesixseveneightnine"))

// 콜백 함수
// var func = function (callback) {
//     console.log('hello');
//     callback();
// }
//
// var callback = function () {
//     console.log('hello2');
// }
//
// func(callback);

// 외계어 사전
// function solution(spell, dic) {
//     let array = [...spell].filter(v => v)
//
//     return array
// }
//
// console.log(solution(["z", "d", "x"], ["def", "dww", "dzx", "loveaw"]))

// 종이 자르기
// function solution(M, N) {
//     return M * N -1;
// }
//
// console.log(solution(2, 2))

// 캐릭터의 좌표
// function solution(keyinput, board) {
//     const answer = [0, 0]
//     const coordinates = [-1, 1, 1, -1]
//     const direction = ["left", "right", "up", "down"]
//     const isSafe = [(board - 1) / 2 ** -1, (board - 1) / 2]
//
//     let leRi = coordinates[direction.indexOf(v)]
//     let upDown = coordinates[direction.indexOf(v)]
//
//
//     return answer
// }
//
// console.log(solution(["left", "right", "up", "right", "right"], [11, 11]))

// 기대값 [2, 1]
// ["down", "down", "down", "down", "down"]	[7, 9]	[0, -4]
// 기대값 [0, -4]
// 제한을 두어야함 -는 (board - 1) / 2 ** -1
// + 는 (board -1) / 2

// Promise
// function getData(callback) {
//     return new Promise(function (resolve, reject){
//
//     })
// }

//         for (let j = 0; j < array[i].length; j++) {
//             if (i === 0 && j > 0) {
//                 result[i][j] = result[i][j - 1] + array[i][j];
//             } else if (i > 0 && j === 0){
//                 result[i][j] = result[i - 1][j] + array[i][j];
//             } else if (i > 0 && j > 0) {
//                 result[i][j] = result[i - 1][j] + result[i][j - 1] - result[i - 1][j - 1] + array[i][j];
//             }
//         }
//     }
//
//     return result;
// }
//
// let array = [[3, 4, 3], [4, 6, 6], [4, 5, 2]];
// console.log(test(array));

// 직사각형 넓이 구하기
// function solution(dots) {
//     let a = []
//     let b = []
//     for(let i = 0; i < dots.length; i++) {
//         a.push(dots[i][0])
//         b.push(dots[i][1])
//     }
//
//     return (Math.max(...a) - Math.min(...a)) * (Math.max(...b) - Math.min(...b))
// }
//
// console.log(solution([[0, 1], [10, 1], [10, 3], [0, 3]]))
// 기대값 20
// (10 - 0) * (3 - 1)

//팩토리얼
// function solution(n) {
//     let m = 1
//     let zegop = 1
//     while(zegop <= n) {
//         zegop = zegop * (m + 1)
//         m++
//     }
//     return m - 1
// }
// console.log(solution(7))

// 로그인 성공
// function solution(id_pw, db) {
//     const [id, pw] = id_pw
//     const ids = db.map(e => e[0])
//     const pws = db.map(e => e[1])
//     let result
//
//     for (let i = 0; i < db.length; i++) {
//         result = (ids.includes(id) === true) && (pw === pws[ids.indexOf(id)]) ? 'login' : ids.includes(id) === true ? "wrong pw" : "fail"
//     }
//
//     // return db.filter(v => v.includes(id_pw)) ? 'login' : id.includes(id_pw[0]) === true ? "wrong pw" : "fail"
//     return result
// }
//
// console.log(solution(["programmer01", "15789"],[["programmer02", "111111"], ["programmer00", "134"], ["programmer01", "1145"]]))
//
// function solution(id_pw, db) {
//   const [id, pw] = id_pw;
//   const map = new Map(db);
//   return map.has(id) ? (map.get(id) === pw ? 'login' : 'wrong pw') : 'fail';
// }

// 보류
// 최빈값
// function solution(good) {
//     const users = ['문미림', '강민정', '권봉주', '김기범']
//
//     const count = good.reduce((acc, cur) => {
//         acc[cur] = (acc[cur] || 0) + 1;
//         return acc
//     }, []);
//
//     // const result = count.indexOf(count.reduce((acc, v) => v === undefined ? '1' : v))
//     const result = count.forEach(obj => {
//         if (typeof Object.keys(obj) !== Number) {
//             obj = 0
//         }
//     })
//
//     return result
// }
//
// console.log(solution(['0','0','0','0','3']))

// 보류
// // 치킨 쿠폰
// function solution(chicken) {
//     let service = 0
//     let coupon = chicken
//     while(coupon >= 10){
//         service += Math.floor(coupon / 10)
//         coupon = Math.floor(coupon / 10) + coupon % 10
//     }
//     return service
// }
// console.log(solution(1081))

// 파괴 되지 않는 건물
// function test(board, skill) {
//     let result = board.map(board => [...board]);
//
//     for (let i = 0; i < skill.length; i++) {
//         let type = skill[i][0]
//         let r1 = skill[i][1]
//         let c1 = skill[i][2]
//         let r2 = skill[i][3]
//         let c2 = skill[i][4]
//         let degree = skill[i][5]
//
//         for(let j = r1; j <= r2; j++) {
//             for(let k = c1; k <= c2; k++) {
//                 if (type === 1) {
//                     result[j][k] -= degree
//                 } else {
//                     result[j][k] += degree
//                 }
//                 console.log(result)
//             }
//         }
//     }
//
//     return result.flat().filter(v => v > 0).length;
// }
//
// let board = [[1,2,3],[4,5,6],[7,8,9]];
// let skill = [[1,1,1,2,2,4],[1,0,0,1,1,2],[2,2,0,2,0,100]];
// console.log(test(board, skill));

// 1차 시도
// function test(board, skill) {
//     let result = board.map(board => [...board]);
//
//     for (let i = 0; i < skill.length; i++) {
//         let type = skill[i][0]
//         let r1 = skill[i][1]
//         let c1 = skill[i][2]
//         let r2 = skill[i][3]
//         let c2 = skill[i][4]
//         let degree = skill[i][5]
//
//         for(let j = r1; j <= r2; j++) {
//             for(let k = c1; k <= c2; k++) {
//                 if (type === 1) {
//                     result[j][k] -= degree
//                 } else {
//                     result[j][k] += degree
//                 }
//                 console.log(result)
//             }
//         }
//     }
//
//     return result.flat().filter(v => v > 0).length;
// }
//
// let board = [[1,2,3],[4,5,6],[7,8,9]];
// let skill = [[1,1,1,2,2,4],[1,0,0,1,1,2],[2,2,0,2,0,100]];
// console.log(test(board, skill));

// board = 건물의 hp
// skill 의 0번 인덱스 1일 때 공격, 2일 때 힐 j
// skill 의 1번 인덱스 가 시작의 j, 2번 인덱스가 시작의 j
// skill의 3번 인덱스가 끝의 j, 4번 인덱스가 끝의 j
// skill의 5번 인덱스가 공격 or 힐량

// for (let i = skill[0][1]; i < skill[0][3]; i++) {
//     for(let j = skill[0][2]; j < skill[0][4]; j++) {
//         if(skill[i][0] === 0) {
//             result.map(v => v - skill[i][5])
//         }
//     }
// }

// 2차 시도
// function solution(board, skill) {
//     let skillArray = Array.from({ length: board.length }, () => Array(board[0].length).fill(0));
//     let sumArray = []
//
//     for(let i = 0; i < skill.length; i++) {
//         for(let j = skill[i][1]; j <= skill[i][3]; j++) {
//             for(let k = skill[i][2]; k <= skill[i][4]; k++) {
//                 if(skill[i][0] === 1) {
//                     skillArray[j][k] -= skill[i][5];
//                 } else {
//                     skillArray[j][k] += skill[i][5];
//                 }
//             }
//         }
//     }
//
//     for(let i = 0; i < skillArray.length; i+=1) {
//         sumArray.push(skillArray[i].map((x, y) => skillArray[i][y] + board[i][y]));
//     }
//
//     return sumArray.flat().filter(e => e > 0).length;
// }
//
// let board = [[1,2,3],[4,5,6],[7,8,9]];
// let skill = [[1,1,1,2,2,4],[1,0,0,1,1,2],[2,2,0,2,0,100]];
// console.log(solution(board, skill));

// 3차 시도
// function solution(board, skill) {
//     let skillArray = Array.from({ length: board.length }, () => Array(board[0].length).fill(0));
//     let sumArray = []
//
//     for(let i = 0; i < skill.length; i++) {
//         if (skill[i][0] === 1 && skill[skill[i][1]][skill[i][2]] === skill[skill[i][3]][skill[i][4]]) {
//             skillArray[skill[i][1]][skill[i][2]] -= skill[i][5]
//         } else if (skill[i][0] === 2 && skill[skill[i][1]][skill[i][2]] === skill[skill[i][3]][skill[i][4]]) {
//             skillArray[skill[i][1]][skill[i][2]] += skill[i][5]
//         } else if (skill[i][0] === 1) {
//             skillArray[skill[i][1]][skill[i][2]] -= skill[i][5]
//             skillArray[skill[i][1]][skill[i][3]] += skill[i][5]
//             skillArray[skill[i][3]][skill[i][1]] += skill[i][5]
//             skillArray[skill[i][3]][skill[i][4]] -= skill[i][5]
//         } else {
//             skillArray[skill[i][1]][skill[i][2]] += skill[i][5]
//             skillArray[skill[i][1]][skill[i][3]] -= skill[i][5]
//             skillArray[skill[i][3]][skill[i][1]] -= skill[i][5]
//             skillArray[skill[i][3]][skill[i][4]] += skill[i][5]
//         }
//     }
//
//     // for(let i = 0; i < skillArray.length; i+=1){
//     //     sumArray.push(skillArray[i].map((x, y) => skillArray[i][y] + board[i][y]));
//     // }
//     //
//     // return sumArray.flat().filter(e => e > 0).length;
//
//     return skillArray
// }
// let board = [[1,2,3],[4,5,6],[7,8,9]];
// let skill = [[1,1,1,2,2,4],[1,0,0,1,1,2],[2,2,0,2,0,100]];
// console.log(solution(board, skill));

// 파괴되지 않는 건물 4차 시도
// function solution(board, skill) {
//     let skillArray = Array.from({ length: board.length + 1 }, () => Array(board[0].length + 1).fill(0))
//     let result = 0
//
//     for(let i = 0; i < skill.length; i++) {
//         if(skill[i][0] === 1) {
//             skillArray[skill[i][1]][skill[i][2]] -= skill[i][5]
//             skillArray[skill[i][3] + 1][skill[i][4] + 1] -= skill[i][5]
//             skillArray[skill[i][1]][skill[i][4] + 1] += skill[i][5]
//             skillArray[skill[i][3] + 1][skill[i][2]] += skill[i][5]
//         } else {
//             skillArray[skill[i][1]][skill[i][2]] += skill[i][5]
//             skillArray[skill[i][3] + 1][skill[i][4] + 1] += skill[i][5]
//             skillArray[skill[i][1]][skill[i][4] + 1] -= skill[i][5]
//             skillArray[skill[i][3] + 1][skill[i][2]] -= skill[i][5]
//         }
//     }
//
//     for(let i = 0; i < skillArray.length; i++) {
//         for(let j = 0; j < skillArray[i].length; j++) {
//             if(i === 0 && j === 0) {
//                 skillArray[i][j] = skillArray[i][j]
//             } else if(i > 0 && j === 0) {
//                 skillArray[i][j] += skillArray[i - 1][j]
//             } else if(i === 0 && j > 0) {
//                 skillArray[i][j] += skillArray[i][j - 1]
//             } else {
//                 skillArray[i][j] += skillArray[i - 1][j] + skillArray[i][j - 1] - skillArray[i - 1][j - 1]
//             }
//         }
//     }
//
//     for(let i = 0; i < board.length; i++) {
//         for(let j = 0; j < board[i].length; j++){
//             board[i][j] += skillArray[i][j]
//             if(board[i][j] > 0) result++
//         }
//     }
//
//     return result
// }

// 저주의 숫자3
// function solution(n) {
//     let town = 0
//     let count = n
//
//     while(count > 0) {
//         town++
//         if (town % 3 === 0 || town % 10 === 3 || Math.floor(town / 10) === 3 || Math.floor((town - 100) / 10) === 3) {
//             continue
//         }
//         count--
//     }
//
//     return town
// }

// 캐릭터의 좌표
// function solution(keyinput, board) {
//     let leftRight = 0
//     let upDown = 0
//     const boardL = (board[0] - 1) / 2 * (-1)
//     const boardR = (board[0] - 1) / 2
//     const boardU = (board[1] - 1) / 2
//     const boardD = (board[1] - 1) / 2 * (-1)
//
//     for(let i = 0; i < keyinput.length; i++) {
//         if(keyinput[i] === "left" && leftRight > boardL) leftRight -= 1
//         else if(keyinput[i] === "right" && leftRight < boardR) leftRight += 1
//         else if(keyinput[i] === "up" && upDown < boardU) upDown += 1
//         else if(keyinput[i] === "down" && upDown > boardD)upDown -= 1
//     }
//
//     return [leftRight, upDown];
// }
//
// console.log(solution(["down", "down", "down", "down", "down"], [7, 9]))

// 유한소수 판별하기
// function solution(a, b) {
//     let result = b / gcd(a, b)
//
//     while(result % 2 === 0) result = result / 2
//     while(result % 5 === 0) result = result / 5
//
//     return result === 1 ? 1 : 2
// }
//
// function gcd(c, d) {
//     return d === 0 ? c : gcd(d, c % d)
// }
//
// console.log(solution(7, 20))

// 외계어 사전
// function solution(spell, dic) {
//     var answer = spell.sort().join('')
//     return dic.map(v => v.split('').sort().join('')).find(e => e === answer) === undefined ? 2 : 1
// }
// function solution(spell, dic) {
//     return dic.some(v => spell.sort() === [...v].sort()) ? 1 : 2;
// }
// console.log(solution(["s", "o", "m", "d"], ["moos", "dzx", "smm", "sunmmo", "som"]))

// ox퀴즈
// function solution(quiz) {
//     var answer = [];
//     let d = quiz.map(e => e.split(' '))
//
//     for (let i = 0; i < d.length; i++) {
//         if (d[i][1] === '-') Number(d[i][0]) - Number(d[i][2]) === Number(d[i][4]) ? answer.push('O') : answer.push('X')
//         else Number(d[i][0]) + Number(d[i][2]) === Number(d[i][4]) ? answer.push('O') : answer.push('X')
//     }
//
//     return answer
// }
// console.log(solution(["3 - 4 = -3", "5 + 6 = 11"], ["X", "O"]))

// 문자열 밀기
// function solution(A, B) {
//     let a = A.split('')
//     let b = B.split('')
//     count = 0
//
//     for (let i = 0; i < A.length; i++) {
//         if (a.join() === b.join()) {
//             count = count
//         } else {
//             a.unshift(a[A.length - 1])
//             a.pop()
//             count++
//         }
//     }
//
//     return count === 0 ? 0 : a.join() === b.join() ? count : -1
// }
//
// console.log(solution("hello", "ohell"))
// // 좋은 예시
// let solution=(a,b)=>(b+b).indexOf(a)
// // b의 문자열과 b문자열을 더한후 a가 포함된 위치를 찾는다. 같은게 없으면 -1을 리턴

// 다항식 더하기
// function solution(polynomial) {
//     const arr = polynomial.split(' ').filter(v => v !== '+')
//     let a = 0
//     let b = 0
//     let result = ''
//
//     for(let i of arr) {
//         if(i.includes('x')) {
//             let temp = i.replace('x', '') || '1'
//             a += Number(temp)
//         } else b += Number(i)
//     }
//
//     if (a !== 0) result = a === 1 ? 'x' : `${a}x`;
//     if (b !== 0) result += (result ? ' + ' : '') + b;
//
//     return result
// }
// console.log(solution("7"));

//  연속된 수의 합
// function solution(num, total) {
//     let answer = []
//     let first = num % 2 === 0 ? Math.floor(total / num) - Math.floor(num / 2) + 1 : Math.floor(total / num) - Math.floor(num / 2)
//
//     for(let i = 0; i < num; i++) {
//         answer.push(first)
//         first++
//     }
//
//     return answer;
// }
// console.log(solution(5, 5))
// // 다른 사람 풀이
// function solution(num, total) {
//     let x = ((2*total/num)+1-num)/2
//     let answer=[];
//     for(let i = 0; i<num;i++){
//         answer.push(x+i)
//     }
//     return answer;
// }

// 최빈값
// function solution(array) {
//     let count = new Map()
//     let result = []
//
//     for (let i = 0; i < array.length; i++) {
//         if (count.has(array[i])) {
//             count.set(array[i], count.get(array[i]) + 1);
//         } else {
//             count.set(array[i], 1);
//         }
//     }
//
//     let max = Math.max(...[...count.values()])
//
//     for (let [key, value] of count) {
//         if (value === max) {
//             result.push(key)
//         }
//     }
//
//     return result.length === 1 ? result[0] : -1
// }
// console.log(solution([1, 2, 2, 3, 3, 4]))
// 다른 사람 풀이
// function solution(array) {
//     let m = new Map();
//     for (let n of array) m.set(n, (m.get(n) || 0)+1);
//     m = [...m].sort((a,b)=>b[1]-a[1]);
//     return m.length === 1 || m[0][1] > m[1][1] ? m[0][0] : -1;
// }
// console.log(solution([1, 2, 2, 3, 3, 4]))

// 특이한 정렬
// function solution(numlist, n) {
//     let answer = []
//     let result = []
//
//     numlist.map(e => answer.push(Math.abs(e - n)))
//
//     let sorted = [...answer].sort((a, b) => a - b)
//
//     for(let i = 0; i < numlist.length; i++){
//         if (numlist.includes(n + sorted[i]) && sorted[i] !== sorted[i-1]) result.push(n + sorted[i])
//         else result.push(n - sorted[i])
//     }
//
//     return result
// }
//
// console.log(solution([1, 2, 3, 4, 5, 6], 4))
// 다른 사람 풀이
// function solution(numlist, n) {
//     return numlist.sort((a, b) => Math.abs(a - n) - Math.abs(b - n) || b - a);
// }

// 없는 숫자 더하기
// function solution(numbers) {
//     return 45 - numbers.reduce((a, b) => a + b)
// }
//
// console.log(solution([1,2,3,4,6,7,8,0]))

// 나머지가 1이 되는 숫자
// function solution(n) {
//     let a = 0
//     while(n % a !== 1) a++
//     return a;
// }
// console.log(solution(12))





// 신규 아이디 추천
function solution(new_id) {
    let result = new_id.toLowerCase().replace(/[^a-z0-9._|-]/g, '').replace(/\.+/g, '.').replace(/^\.|\.$/g, '').replace(/^$/g, 'a').substr(0, 15).replace(/\.$/g, '')
    while(result.length < 3) result += result.substr(-1)
    return result
}
console.log(solution("z-+.^."))
// 다른 사람 풀이
// return result.length > 2 ? result : result + result.charAt(result.length - 1).repeat(3 - result.length)


